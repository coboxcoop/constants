const path = require('path')
const envPaths = require('env-paths')
const os = require('os')

const serverPort = 9112
const seederPort = 9111
const udpPort = 8999

const paths = envPaths('cobox', { suffix: '' })

const storage = path.join(paths.config)
const mount = path.join(os.homedir(), 'cobox')
const seederStorage = path.join(paths.data, 'seeder')

module.exports = {
  storage,
  seederStorage,
  mount,
  keyIds: {
    identity: 0,
    log: 0,
    metadata: 1,
    content: 2
  }
}

module.exports.serverDefaults = {
  storage,
  port: serverPort,
  udpPort,
  mount,
  ui: true
}

module.exports.seederDefaults = {
  storage: seederStorage,
  port: seederPort,
  udpPort
}
